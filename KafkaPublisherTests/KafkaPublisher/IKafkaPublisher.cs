﻿using Avro.Specific;

namespace KafkaPublisher
{
    public interface IKafkaPublisher<TValue> where TValue : ISpecificRecord
    {
        Task PublishAsync(string key, TValue data, string topic);
    }
}
