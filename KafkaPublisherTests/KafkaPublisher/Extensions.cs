﻿using Avro.Specific;
using System.Globalization;
using System.Reflection;

namespace KafkaPublisher
{
    public static class Extensions
    {
        public static TEvent WithDefaultEventHeader<TEvent>(this TEvent e) where TEvent : ISpecificRecord
        {
            var property = typeof(TEvent).GetProperty("EventHeader");
            var value = property.GetValue(e);
            if (value == null)
            {
                value = Activator.CreateInstance(property.PropertyType);
                property.SetValue(e, value);
            }

            var eventHeader = (dynamic)value;
            eventHeader.EventId = eventHeader.EventId ?? Guid.NewGuid().ToString();
            eventHeader.Timestamp = eventHeader.Timestamp ?? DateTime.UtcNow.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffZ", CultureInfo.InvariantCulture);
            eventHeader.CorrelationId = eventHeader.CorrelationId ?? Guid.NewGuid().ToString();
            eventHeader.Source = eventHeader.Source ?? Assembly.GetEntryAssembly()?.FullName;
            eventHeader.Metadata = eventHeader.Metadata ?? new Dictionary<string, string>();
            return e;
        }
    }
}
