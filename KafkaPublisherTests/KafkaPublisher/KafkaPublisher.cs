﻿using Avro.Specific;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;
using Microsoft.Extensions.Logging;
using System.Text;

namespace KafkaPublisher
{
    public class KafkaPublisher<TValue> : IKafkaPublisher<TValue>, IDisposable where TValue : ISpecificRecord
    {
        private readonly ILogger<KafkaPublisher<TValue>> _logger;
        private readonly IProducer<string, TValue> _producer;
        private readonly CachedSchemaRegistryClient _schemaRegistryClient;
        private bool disposed;

        public KafkaPublisher(ILogger<KafkaPublisher<TValue>> logger, bool setKeySerializer = true)
        {
            _logger = logger;
            _schemaRegistryClient = new CachedSchemaRegistryClient(
                    new SchemaRegistryConfig
                    {
                        Url = "localhost:8081"
                    });

            var producerBuilder = new ProducerBuilder<string, TValue>(new ProducerConfig { BootstrapServers = "localhost:9092" })
                .SetValueSerializer(new AvroSerializer<TValue>(_schemaRegistryClient, new AvroSerializerConfig { AutoRegisterSchemas = true, SubjectNameStrategy = SubjectNameStrategy.Record }).AsSyncOverAsync())
                .SetLogHandler(OnKafkaLog)
                .SetErrorHandler(OnKafkaError);

            if (setKeySerializer)
            {
                producerBuilder = producerBuilder.SetKeySerializer(new AvroSerializer<string>(_schemaRegistryClient, new AvroSerializerConfig { AutoRegisterSchemas = true }).AsSyncOverAsync());
            }

            _producer = producerBuilder.Build();
        }

        public async Task PublishAsync(string key, TValue data, string topic)
        {
            if (data == null)
            {
                return;
            }

            var message = new Message<string, TValue>
            {
                Value = data
            };

            if (key != null)
            {
                message.Key = key;
            }

            message.Headers ??= [];
            message.Headers.Add("MessageType", Encoding.UTF8.GetBytes(typeof(TValue).FullName));

            await _producer.ProduceAsync(topic, message);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                _producer.Flush();
                _producer.Dispose();
                _schemaRegistryClient.Dispose();
            }

            disposed = true;
        }

        private void OnKafkaLog<TEvent>(IProducer<string, TEvent> producer, LogMessage message)
        {
            _logger.LogInformation("Message from Kafka producer: {Message}.", message.Message);
        }

        private void OnKafkaError<TEvent>(IProducer<string, TEvent> producer, Error error)
        {
            _logger.LogError("Error while producing Kafka message: {ErrorCode}-{ErrorMessage}", error.Code, error.Reason);
        }
    }
}
