﻿using Microsoft.Extensions.Logging;
using KafkaPublisher;
using Zip.MessageTypes;

internal class Program
{
    private static async Task Main(string[] args)
    {
        Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "local");

        var loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
        var logger = loggerFactory.CreateLogger<KafkaPublisher<Heartbeat>>();
        var kafkaPublisher = new KafkaPublisher<Heartbeat>(logger);

        var message = new Heartbeat
        {
            ApplicationInstance = "test-producer"
        }.WithDefaultEventHeader();

        logger.LogInformation("Starting to publish");

        await kafkaPublisher.PublishAsync(message.ExtractPartitionKey(), message, message.ExtractTopicNameForEvent());

        logger.LogInformation("Finished publishing");
    }
}