using KafkaPublisher;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;
using Zip.MessageTypes;
using Zip.MessageTypes.Accounts;
using Zip.MessageTypes.CoreProduct;
using Zip.MessageTypes.DataRisk;

namespace KafkaPublisherTests
{
    public class PublishMessageTest
    {
        public PublishMessageTest()
        {
            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "local");
        }

        [Fact]
        public async Task PublishHeartbeatEventAsync()
        {
            var publisher = new KafkaPublisher<Heartbeat>(
                new Mock<ILogger<KafkaPublisher<Heartbeat>>>().Object);

            var message = new Heartbeat
            {
                ApplicationInstance = "test-producer"
            }.WithDefaultEventHeader();

            await publisher.PublishAsync(message.ExtractPartitionKey(), message, message.ExtractTopicNameForEvent());
        }

        [Fact]
        public async Task PublishLmsTransactionAuthorisedEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "RZ/Ni2vvb+8FRr9Ro7ZpRdk0RxtRjuCifqODp+qKLZjcQTN1NXYGvMchtNEBCmDgDN1o9CQRsmZtSKkACBeZrPzG7VxSqipRgf6RZ+a7212CR5M62+mfot24cnonXCLma4a8YggoEB0h+ctzStzPrpnrfidZJtz5k+PArWw/okguWsQIuLYs7NPpaKyOpakUBKCyu9gvO/4wU5S6lrQkywYw2QLtmN2/QCcc1+/NHDDW51Tvc4pQ0TAdYysdl8Q/uayxJEuAIKTk3qBcrpEMZ0k9tb/6yDrtowGrqYr8IwqcdV8vnd1FD7xT/HKcA3gcPK0bAuz90RHKX/QxjvaObA==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"e6f23d54-4d2f-450e-83f2-b535b1d58e7d#8318050\",\"AccountId\":2224589,\"LmsTransactionId\":\"204631043\",\"TransactionId\":321143201,\"ConsumerId\":6484775,\"Amount\":100.000000,\"ExternalReference\":\"cd1b0de3-32cc-479c-96b5-0a93ec1ec701\",\"TransactionHistoryType\":\"Authorised\",\"Id\":\"75697692-90f6-49da-8063-e3802e5c7276\",\"TimeStamp\":\"2021-05-18T23:19:11.6153871Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsTransactionCancelledEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "RZ/Ni2vvb+8FRr9Ro7ZpRdk0RxtRjuCifqODp+qKLZjcQTN1NXYGvMchtNEBCmDgDN1o9CQRsmZtSKkACBeZrPzG7VxSqipRgf6RZ+a7212CR5M62+mfot24cnonXCLma4a8YggoEB0h+ctzStzPrpnrfidZJtz5k+PArWw/okguWsQIuLYs7NPpaKyOpakUBKCyu9gvO/4wU5S6lrQkywYw2QLtmN2/QCcc1+/NHDDW51Tvc4pQ0TAdYysdl8Q/uayxJEuAIKTk3qBcrpEMZ0k9tb/6yDrtowGrqYr8IwqcdV8vnd1FD7xT/HKcA3gcPK0bAuz90RHKX/QxjvaObA==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"e6f23d54-4d2f-450e-83f2-b535b1d58e7d#8318050\",\"AccountId\":2224589,\"LmsTransactionId\":\"204631043\",\"TransactionId\":321143202,\"ParentTransactionHistoryId\":321143201,\"ConsumerId\":6484775,\"Amount\":-100.000000,\"ExternalReference\":\"cd1b0de3-32cc-479c-96b5-0a93ec1ec701\",\"TransactionHistoryType\":\"Cancelled\",\"Id\":\"75697692-90f6-49da-8063-e3802e5c7276\",\"TimeStamp\":\"2021-05-18T23:19:11.6153871Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsTransactionRefundedEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "qawf7RnFaLeQgen1VK417o2KLrXFYd4cGViWrWBiX21bWDfZMtnZrWSsgm0eDJuY606p0mX7QlRxBo42+7zVLuUxfHbwvfOk+AjI2pfSxnYfRmQX8qQDi5pzgOX54pnJ9Yx4OhDX4zse7bNUurWZkuo9a55pDAPJZNw6JKP0M3wwMtSD159QgkjLy9+RC9VN3zFrA+1EfRFUr7HsfIEvF7OXULwcB61toCHVbMQJEa/63PYS42N7c8jEGoe1CKFSddt/RYVKM8V54yiP9EkrzcFoOhDC/drCfL3XqTycYZTPVCVRMOzm+foTcpI2ey12xbXejTKhJBvWhwBe+O84eQ==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"0HM91CFIM0GPK:00000001\",\"AccountId\":2224589,\"LmsTransactionId\":\"2157134\",\"TransactionId\":321143208,\"ParentTransactionHistoryId\":321143204,\"ConsumerId\":185188,\"Amount\":-10,\"ExternalReference\":\"BI0871892\",\"TransactionHistoryType\":\"Refunded\",\"Id\":\"2e2c2dc1-3aaf-4a96-927b-4e531926af11\",\"TimeStamp\":\"2021-05-28T01:52:19.2755978Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsTransactionInterestEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "RZ/Ni2vvb+8FRr9Ro7ZpRdk0RxtRjuCifqODp+qKLZjcQTN1NXYGvMchtNEBCmDgDN1o9CQRsmZtSKkACBeZrPzG7VxSqipRgf6RZ+a7212CR5M62+mfot24cnonXCLma4a8YggoEB0h+ctzStzPrpnrfidZJtz5k+PArWw/okguWsQIuLYs7NPpaKyOpakUBKCyu9gvO/4wU5S6lrQkywYw2QLtmN2/QCcc1+/NHDDW51Tvc4pQ0TAdYysdl8Q/uayxJEuAIKTk3qBcrpEMZ0k9tb/6yDrtowGrqYr8IwqcdV8vnd1FD7xT/HKcA3gcPK0bAuz90RHKX/QxjvaObA==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"e6f23d54-4d2f-450e-83f2-b535b1d58e7d#8318050\",\"AccountId\":2224589,\"LmsTransactionId\":\"204631043\",\"TransactionId\":321143206,\"ConsumerId\":6484775,\"Amount\":10.00,\"ExternalReference\":\"cd1b0de3-32cc-479c-96b5-0a93ec1ec701\",\"TransactionHistoryType\":\"Interest\",\"Id\":\"75697692-90f6-49da-8063-e3802e5c7276\",\"TimeStamp\":\"2021-05-18T23:19:11.6153871Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsTransactionInterestRefundedEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "RZ/Ni2vvb+8FRr9Ro7ZpRdk0RxtRjuCifqODp+qKLZjcQTN1NXYGvMchtNEBCmDgDN1o9CQRsmZtSKkACBeZrPzG7VxSqipRgf6RZ+a7212CR5M62+mfot24cnonXCLma4a8YggoEB0h+ctzStzPrpnrfidZJtz5k+PArWw/okguWsQIuLYs7NPpaKyOpakUBKCyu9gvO/4wU5S6lrQkywYw2QLtmN2/QCcc1+/NHDDW51Tvc4pQ0TAdYysdl8Q/uayxJEuAIKTk3qBcrpEMZ0k9tb/6yDrtowGrqYr8IwqcdV8vnd1FD7xT/HKcA3gcPK0bAuz90RHKX/QxjvaObA==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"e6f23d54-4d2f-450e-83f2-b535b1d58e7d#8318050\",\"AccountId\":2224589,\"LmsTransactionId\":\"204631043\",\"TransactionId\":321143202,\"ConsumerId\":6484775,\"Amount\":-10.00,\"ExternalReference\":\"cd1b0de3-32cc-479c-96b5-0a93ec1ec701\",\"TransactionHistoryType\":\"RefundedInterest\",\"Id\":\"75697692-90f6-49da-8063-e3802e5c7276\",\"TimeStamp\":\"2021-05-18T23:19:11.6153871Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsTransactionFeeEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "RZ/Ni2vvb+8FRr9Ro7ZpRdk0RxtRjuCifqODp+qKLZjcQTN1NXYGvMchtNEBCmDgDN1o9CQRsmZtSKkACBeZrPzG7VxSqipRgf6RZ+a7212CR5M62+mfot24cnonXCLma4a8YggoEB0h+ctzStzPrpnrfidZJtz5k+PArWw/okguWsQIuLYs7NPpaKyOpakUBKCyu9gvO/4wU5S6lrQkywYw2QLtmN2/QCcc1+/NHDDW51Tvc4pQ0TAdYysdl8Q/uayxJEuAIKTk3qBcrpEMZ0k9tb/6yDrtowGrqYr8IwqcdV8vnd1FD7xT/HKcA3gcPK0bAuz90RHKX/QxjvaObA==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"e6f23d54-4d2f-450e-83f2-b535b1d58e7d#8318050\",\"AccountId\":2224589,\"LmsTransactionId\":\"204631043\",\"TransactionId\":321143205,\"ConsumerId\":6484775,\"Amount\":6.00,\"ExternalReference\":\"cd1b0de3-32cc-479c-96b5-0a93ec1ec701\",\"TransactionHistoryType\":\"MonthlyFee\",\"Id\":\"75697692-90f6-49da-8063-e3802e5c7276\",\"TimeStamp\":\"2021-05-18T23:19:11.6153871Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsTransactionCapturedEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "RZ/Ni2vvb+8FRr9Ro7ZpRdk0RxtRjuCifqODp+qKLZjcQTN1NXYGvMchtNEBCmDgDN1o9CQRsmZtSKkACBeZrPzG7VxSqipRgf6RZ+a7212CR5M62+mfot24cnonXCLma4a8YggoEB0h+ctzStzPrpnrfidZJtz5k+PArWw/okguWsQIuLYs7NPpaKyOpakUBKCyu9gvO/4wU5S6lrQkywYw2QLtmN2/QCcc1+/NHDDW51Tvc4pQ0TAdYysdl8Q/uayxJEuAIKTk3qBcrpEMZ0k9tb/6yDrtowGrqYr8IwqcdV8vnd1FD7xT/HKcA3gcPK0bAuz90RHKX/QxjvaObA==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"e6f23d54-4d2f-450e-83f2-b535b1d58e7d#8318050\",\"AccountId\":2224589,\"LmsTransactionId\":\"204631043\",\"OrderId\":5,\"TransactionId\":321143204,\"ConsumerId\":6484775,\"Amount\":100.00,\"ExternalReference\":\"cd1b0de3-32cc-479c-96b5-0a93ec1ec701\",\"TransactionHistoryType\":\"Captured\",\"Id\":\"75697692-90f6-49da-8063-e3802e5c7276\",\"TimeStamp\":\"2021-05-18T23:19:11.6153871Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsTransactionPaymentEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "RZ/Ni2vvb+8FRr9Ro7ZpRdk0RxtRjuCifqODp+qKLZjcQTN1NXYGvMchtNEBCmDgDN1o9CQRsmZtSKkACBeZrPzG7VxSqipRgf6RZ+a7212CR5M62+mfot24cnonXCLma4a8YggoEB0h+ctzStzPrpnrfidZJtz5k+PArWw/okguWsQIuLYs7NPpaKyOpakUBKCyu9gvO/4wU5S6lrQkywYw2QLtmN2/QCcc1+/NHDDW51Tvc4pQ0TAdYysdl8Q/uayxJEuAIKTk3qBcrpEMZ0k9tb/6yDrtowGrqYr8IwqcdV8vnd1FD7xT/HKcA3gcPK0bAuz90RHKX/QxjvaObA==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"e6f23d54-4d2f-450e-83f2-b535b1d58e7d#8318050\",\"AccountId\":2224589,\"LmsTransactionId\":\"204631043\",\"TransactionId\":321143207,\"ConsumerId\":6484775,\"Amount\":20.00,\"ExternalReference\":\"cd1b0de3-32cc-479c-96b5-0a93ec1ec701\",\"TransactionHistoryType\":\"Payment\",\"Id\":\"75697692-90f6-49da-8063-e3802e5c7276\",\"TimeStamp\":\"2021-05-18T23:19:11.6153871Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }
        
        [Fact]
        public async Task PublishLmsNonInstallmentRepaymentEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "qawf7RnFaLeQgen1VK417o2KLrXFYd4cGViWrWBiX21bWDfZMtnZrWSsgm0eDJuY606p0mX7QlRxBo42+7zVLuUxfHbwvfOk+AjI2pfSxnYfRmQX8qQDi5pzgOX54pnJ9Yx4OhDX4zse7bNUurWZkuo9a55pDAPJZNw6JKP0M3wwMtSD159QgkjLy9+RC9VN3zFrA+1EfRFUr7HsfIEvF7OXULwcB61toCHVbMQJEa/63PYS42N7c8jEGoe1CKFSddt/RYVKM8V54yiP9EkrzcFoOhDC/drCfL3XqTycYZTPVCVRMOzm+foTcpI2ey12xbXejTKhJBvWhwBe+O84eQ==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"0HM91CFIM0GPK:00000001\",\"InstallmentAccount\":false,\"DestinationAccount\":\"1234\",\"AccountId\":2224589,\"OrderId\":5,\"LmsTransactionId\":\"2157101\",\"TransactionId\":2630401,\"ConsumerId\":185188,\"Amount\":50.000000,\"ExternalReference\":\"BI0871892\",\"TransactionHistoryType\":\"Payment\",\"Id\":\"2e2c2dc1-3aaf-4a96-927b-4e531926af11\",\"TimeStamp\":\"2021-05-28T01:52:19.2755978Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsNonInstallmentPartialRepaymentAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "qawf7RnFaLeQgen1VK417o2KLrXFYd4cGViWrWBiX21bWDfZMtnZrWSsgm0eDJuY606p0mX7QlRxBo42+7zVLuUxfHbwvfOk+AjI2pfSxnYfRmQX8qQDi5pzgOX54pnJ9Yx4OhDX4zse7bNUurWZkuo9a55pDAPJZNw6JKP0M3wwMtSD159QgkjLy9+RC9VN3zFrA+1EfRFUr7HsfIEvF7OXULwcB61toCHVbMQJEa/63PYS42N7c8jEGoe1CKFSddt/RYVKM8V54yiP9EkrzcFoOhDC/drCfL3XqTycYZTPVCVRMOzm+foTcpI2ey12xbXejTKhJBvWhwBe+O84eQ==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"0HM91CFIM0GPK:00000001\",\"InstallmentAccount\":false,\"DestinationAccount\":\"1234\",\"AccountId\":2224589,\"OrderId\":5,\"LmsTransactionId\":\"2157101\",\"TransactionId\":2630401,\"ConsumerId\":185188,\"Amount\":8.000000,\"ExternalReference\":\"BI0871892\",\"TransactionHistoryType\":\"Payment\",\"Id\":\"2e2c2dc1-3aaf-4a96-927b-4e531926af11\",\"TimeStamp\":\"2021-05-28T01:52:19.2755978Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishLmsNonInstallmentRepaymentRefundEventAsync()
        {
            var publisher = new KafkaPublisher<SqsMessage>(new Mock<ILogger<KafkaPublisher<SqsMessage>>>().Object, false);
            var sqsMessage = new SqsMessage
            {
                MessageId = Guid.NewGuid().ToString(),
                Type = "Notification",
                Signature = "qawf7RnFaLeQgen1VK417o2KLrXFYd4cGViWrWBiX21bWDfZMtnZrWSsgm0eDJuY606p0mX7QlRxBo42+7zVLuUxfHbwvfOk+AjI2pfSxnYfRmQX8qQDi5pzgOX54pnJ9Yx4OhDX4zse7bNUurWZkuo9a55pDAPJZNw6JKP0M3wwMtSD159QgkjLy9+RC9VN3zFrA+1EfRFUr7HsfIEvF7OXULwcB61toCHVbMQJEa/63PYS42N7c8jEGoe1CKFSddt/RYVKM8V54yiP9EkrzcFoOhDC/drCfL3XqTycYZTPVCVRMOzm+foTcpI2ey12xbXejTKhJBvWhwBe+O84eQ==",
                Subject = "LmsTransactionCreatedEvent",
                UnsubscribeURL = "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent:299ffd33-4434-41bc-ba09-890b9f476e9c",
                TopicArn = "arn:aws:sns:ap-southeast-2:381371729123:zip-services-accounts-production-lmstransactioncreatedevent",
                Timestamp = DateTimeOffset.UtcNow.ToString("o"),
                Message = "{\"CorrelationId\":\"0HM91CFIM0GPK:00000001\",\"InstallmentAccount\":false,\"DestinationAccount\":\"1234\",\"AccountId\":2224589,\"LmsTransactionId\":\"2157102\",\"TransactionId\":2630406,\"ParentTransactionHistoryId\":2630401,\"ConsumerId\":185188,\"Amount\":-25.550000,\"ExternalReference\":\"BI0871892\",\"TransactionHistoryType\":\"RepaymentRefund\",\"Id\":\"2e2c2dc1-3aaf-4a96-927b-4e531926af11\",\"TimeStamp\":\"2021-05-28T01:52:19.2755978Z\",\"MessageAttributes\":{\"transactionType\":{\"StringValue\":\"purchase\",\"DataType\":\"String\"}}}"
            };
            await publisher.PublishAsync(null, sqsMessage, Topics.AccountsLmsCreated);
        }

        [Fact]
        public async Task PublishInstallmentCapturedAsync()
        {
            var publisher = new KafkaPublisher<InstallmentCaptured>(new Mock<ILogger<KafkaPublisher<InstallmentCaptured>>>().Object);
            var installmentCaptured = new InstallmentCaptured
            {
                AccountId = 2224589,
                DateAdded = "2021-06-09",
                InstallmentDate = "2021-06-09",
                ExternalId = 2,
                ExternalReference = Guid.NewGuid().ToString(),
                PaymentBatchId = Guid.NewGuid().ToString(),
                State = "Captured",
                OrderId = 2,
                InstallmentScheduleId = 1,
                InstallmentNo = 0,
                Amount = "18.75",
                FeeAmount = "0",
                Retries = 0
            }.WithDefaultEventHeader();
            await publisher.PublishAsync(installmentCaptured.ExtractPartitionKey(), installmentCaptured, Topics.AccountsInstallmentCaptured);
        }

        [Fact]
        public async Task PublishEmailConfirmationEvent()
        {
            var publisher = new KafkaPublisher<EmailConfirmation>(new Mock<ILogger<KafkaPublisher<EmailConfirmation>>>().Object);
            var emailConfirmation = new EmailConfirmation
            {
                Email = "test@zip.co",
                CountryId = "AU",
                Token = "token",
                ConsumerId = 1,
                UserId = "user"
            }.WithDefaultEventHeader();
            await publisher.PublishAsync(emailConfirmation.ExtractPartitionKey(), emailConfirmation, Topics.EmailConfirmation);
        }
    }
}
