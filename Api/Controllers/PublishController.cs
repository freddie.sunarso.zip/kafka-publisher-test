using KafkaPublisher;
using Microsoft.AspNetCore.Mvc;
using Zip.MessageTypes;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PublishController(ILogger<PublishController> logger, IKafkaPublisher<Heartbeat> kafkaPublisher) : ControllerBase
    {
        [HttpGet(Name = "PublishHeartbeat")]
        public async Task PublishHeartbeat()
        {
            logger.LogInformation("Starting to publish");

            var message = new Heartbeat
            {
                ApplicationInstance = "test-producer"
            }.WithDefaultEventHeader();

            await kafkaPublisher.PublishAsync(message.ExtractPartitionKey(), message, message.ExtractTopicNameForEvent());

            logger.LogInformation("Finished publishing");
        }
    }
}
